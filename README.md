# Laravel Project

Welcome to the Laravel Project! This is a brief overview of the project and how to get started.

## Description

This project is built using the Laravel framework. It includes functionality for user registration, generating unique links, playing a game, and viewing game history.

## Features

- User registration
- Generation of unique links
- Game play with win/lose outcomes
- Viewing game history

## Installation

1. Clone the repository:
    - git clone https://gitlab.com/vladikkk1/devporttest.git
    - cd devporttest  

2. Install Composer dependencies
    - composer install
    
3. Create a .env file by copying the .env.example file and updating database settings
    - cp .env.example .env
    - php artisan key:generate       

4. Run database migrations
    - php artisan migrate
       
5. Start the development server
    - php artisan serve
    

6. Visit http://localhost:8000 in your browser to access the application.

## Usage
- Register a new user by filling out the registration form
- After registration, a unique link will be generated for the user.
- Play the game using the "Play" button to win or lose.
- View game history by clicking the "History" button.
