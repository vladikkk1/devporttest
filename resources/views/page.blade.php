@extends('layouts.main')


@section('content')
    <div class="container mt-5">
        <h2>Ваша уникальная страница - {{$uniqueLink}}</h2>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <h4>Генерировать новый уникальный линк</h4>
                <form action="{{route('unique.generate' , $user->id)}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-primary">Генерировать</button>
                </form>
                <h4 class="mt-4">Деактивировать уникальный линк</h4>
                <form action="{{route('unique.deactivate' , $user->id)}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-danger">Деактивировать</button>
                </form>
            </div>
            <div class="col-md-6">
                <h4>Imfeelinglucky</h4>
                <button id="playGameButton" class="btn btn-success">Играть</button>
                <p>Результат: <span id="result">-</span></p>
                <p>Сумма выигрыша: <span id="winAmount">-</span></p>
            </div>
        </div>

        <button id="historyButton" class="btn btn-warning mt-5">История</button>
        <table class="table" id="historyTable">
            <thead>
            <tr>
                <th>Дата</th>
                <th>Результат</th>
                <th>Сумма выигрыша</th>
            </tr>
            </thead>
            <tbody id="historyTableBody">
            </tbody>
        </table>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#playGameButton').click(function () {
                $.ajax({
                    url: 'play/{{$user->id}}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        $('#result').text(response.is_win ? 'Win' : 'Lose');
                        $('#winAmount').text(response.win_amount.toFixed(2));
                    },
                    error: function () {
                        $('#result').text('Ошибка при выполнении запроса');
                        $('#winAmount').text('-');
                    }
                });
            });



            $('#historyButton').click(function () {
                $.ajax({
                    url: '/history/{{$user->id}}', // Замените на ваш роут для получения истории
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        $('#historyTableBody').empty();
                        if (response.length === 0) {
                            $('#historyTableBody').append('<tr><td colspan="3">Сыграйте в игру</td></tr>');
                        } else {
                            response.forEach(function (item) {
                                const formattedDate = new Date(item.created_at).toLocaleString();
                                $('#historyTableBody').append('<tr><td>' + formattedDate + '</td><td>' + (item.is_win ? 'Win' : 'Lose') + '</td><td>' + item.win_amount + '</td></tr>');
                            });
                        }
                    },
                    error: function () {
                        $('#historyTableBody').empty();
                        $('#historyTableBody').append('<tr><td colspan="3">Ошибка при получении истории</td></tr>');
                    }
                });
            });
        });
    </script>
@endsection
