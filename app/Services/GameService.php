<?php

namespace App\Services;

use App\Models\GameResult;
use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Services\Interfaces\GameServiceInterface;
use Illuminate\Database\Eloquent\Collection;

class GameService implements GameServiceInterface
{

    protected GameRepositoryInterface $gameResultRepository;

    /**
     * Coefficients for calculating win amounts.
     */
    const WIN_AMOUNT_COEFFICIENTS = [
        900 => 0.7,
        600 => 0.5,
        300 => 0.3,
        0 => 0.1,
    ];

    public function __construct(GameRepositoryInterface $gameResultRepository)
    {
        $this->gameResultRepository = $gameResultRepository;
    }

    /**
     * Generate a random number between 1 and 1000.
     *
     * @return int
     */
    public function generateRandomNumber(): int
    {
        return rand(1, 1000);
    }

    /**
     * Play the game and store the result.
     *
     * @param int $userId
     * @return GameResult
     */
    public function playGame(int $userId): GameResult
    {
        $randomNumber = $this->generateRandomNumber();
        $isWin = $randomNumber % 2 === 0;

        $winAmount = 0;
        if ($isWin) {
            foreach (self::WIN_AMOUNT_COEFFICIENTS as $threshold => $coefficient) {
                if ($randomNumber > $threshold) {
                    $winAmount = $randomNumber * $coefficient;
                    break;
                }
            }
        }


        return $this->gameResultRepository->create([
            'user_id' => $userId,
            'random_number' => $randomNumber,
            'is_win' => $isWin,
            'win_amount' => $winAmount,
        ]);

    }

    /**
     * Find game results by user ID.
     *
     * @param int $userId
     * @return GameResult
     */
    public function findByUserId(int $userId): GameResult
    {
        return $this->gameResultRepository->findByUserId($userId);
    }

    /**
     * Get the game history for a user.
     *
     * @param int $userId
     * @return Collection
     */
    public function history(int $userId): Collection
    {
        return $this->gameResultRepository->history($userId);
    }

}
