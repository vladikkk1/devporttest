<?php

namespace App\Services\Interfaces;

use App\Models\User;

interface UniqueLinkServiceInterface
{
    public function generateUniqueLink(int $userId): string;

    public function validateAndRetrieveUser(string $uniqueLink): ?User;

    public function deactivateLink(int $userId): void;
}
