<?php

namespace App\Services\Interfaces;

use App\Models\GameResult;
use Illuminate\Database\Eloquent\Collection;

interface GameServiceInterface
{

    public function generateRandomNumber(): int;

    public function playGame(int $userId): GameResult;

    public function findByUserId(int $userId): GameResult;

    public function history(int $userId): Collection;
}
