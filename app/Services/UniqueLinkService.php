<?php

namespace App\Services;


use App\Models\User;
use App\Repositories\Interfaces\UniqueLinkRepositoryInterface;
use App\Services\Interfaces\UniqueLinkServiceInterface;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class UniqueLinkService implements UniqueLinkServiceInterface
{

    protected UniqueLinkRepositoryInterface $uniqueLinkRepository;

    public function __construct(UniqueLinkRepositoryInterface $uniqueLinkRepository)
    {
        $this->uniqueLinkRepository = $uniqueLinkRepository;
    }

    /**
     * Generate a unique link for the given user.
     *
     * @param int $userId
     * @return string
     */
    public function generateUniqueLink(int $userId): string
    {
        $existingLink = $this->uniqueLinkRepository->findByUserId($userId);
        if ($existingLink) {
            $this->updateExistingLink($existingLink);
        } else {
            $existingLink = $this->createNewLink($userId);
        }

        return $existingLink->unique_link;
    }

    protected function updateExistingLink($link)
    {
        $this->uniqueLinkRepository->updateExistingLink($link);
    }

    protected function createNewLink($userId)
    {
        $uniqueCode = Str::random(10);
        $expiresAt = Carbon::now()->addDays(7);

        return $this->uniqueLinkRepository->create([
            'user_id' => $userId,
            'unique_link' => $uniqueCode,
            'expires_at' => $expiresAt,
        ]);
    }

    /**
     * Validate the unique link and retrieve the associated user.
     *
     * @param string $uniqueLink
     * @return User|null
     */
    public function validateAndRetrieveUser(string $uniqueLink): ?User
    {
        $uniqueLinkModel = $this->uniqueLinkRepository->findByUniqueLink($uniqueLink);

        if (!$uniqueLinkModel || $uniqueLinkModel->expires_at < now()) {
            return null;
        }

        return $uniqueLinkModel->user;
    }

    /**
     * Deactivate the unique link for the given user.
     *
     * @param int $userId
     */
    public function deactivateLink(int $userId): void
    {
        $uniqueLinkModel = $this->uniqueLinkRepository->findByUserId($userId);
        $this->uniqueLinkRepository->deactivateLink($uniqueLinkModel);
    }
}
