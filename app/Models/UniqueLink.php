<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UniqueLink extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'unique_link', 'expires_at'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
