<?php

namespace App\Repositories\Interfaces;

use App\Models\GameResult;
use Illuminate\Database\Eloquent\Collection;

interface GameRepositoryInterface
{

    public function create(array $data): GameResult;

    public function findByUserId(int $userId): GameResult;

    public function history(int $userId): Collection;

}
