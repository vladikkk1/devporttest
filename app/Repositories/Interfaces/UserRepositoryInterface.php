<?php

namespace App\Repositories\Interfaces;

use App\Models\User;
use Illuminate\Support\Carbon;

interface UserRepositoryInterface
{
    public function create(array $data): User;

}
