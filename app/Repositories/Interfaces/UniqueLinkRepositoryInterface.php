<?php

namespace App\Repositories\Interfaces;

use App\Models\UniqueLink;

interface UniqueLinkRepositoryInterface
{
    public function findByUserId(int $userId): ?UniqueLink;

    public function create(array $data): UniqueLink;

    public function findByUniqueLink(string $uniqueLink): ?UniqueLink;

    public function updateExistingLink(UniqueLink $uniqueLink): void;

    public function deactivateLink(UniqueLink $uniqueLink): void;
}
