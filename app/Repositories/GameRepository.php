<?php

namespace App\Repositories;

use App\Models\GameResult;
use App\Repositories\Interfaces\GameRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class GameRepository implements GameRepositoryInterface
{

    /**
     * Number of last game results to retrieve.
     */
    const COUNTER_LAST_RESULTS = 3;

    /**
     * Create a new game result entry.
     *
     * @param array $data
     * @return GameResult
     */
    public function create(array $data): GameResult
    {
        return GameResult::create($data);
    }

    /**
     * Find game results by user ID.
     *
     * @param int $userId
     * @return GameResult
     */
    public function findByUserId(int $userId): GameResult
    {
        return GameResult::find($userId);
    }

    /**
     * Retrieve the game history for a user.
     *
     * @param int $userId
     * @return Collection
     */
    public function history(int $userId): Collection
    {
        return GameResult::query()
            ->where('user_id', $userId)
            ->orderByDesc('created_at')
            ->take(self::COUNTER_LAST_RESULTS)
            ->get();
    }
}
