<?php

namespace App\Repositories;

use App\Models\UniqueLink;
use App\Repositories\Interfaces\UniqueLinkRepositoryInterface;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class UniqueLinkRepository implements UniqueLinkRepositoryInterface
{

    /**
     * Find a unique link by user ID.
     *
     * @param int $userId
     * @return UniqueLink|null
     */
    public function findByUserId(int $userId): ?UniqueLink
    {
        return UniqueLink::where('user_id', $userId)->first();
    }

    /**
     * Create a new unique link entry.
     *
     * @param array $data
     * @return UniqueLink
     */
    public function create(array $data): UniqueLink
    {
        return UniqueLink::create($data);
    }

    /**
     * Find a unique link by its value.
     *
     * @param string $uniqueLink
     * @return UniqueLink|null
     */
    public function findByUniqueLink(string $uniqueLink): ?UniqueLink
    {
        return UniqueLink::where('unique_link', $uniqueLink)->first();
    }

    /**
     * Update an existing unique link.
     *
     * @param UniqueLink $uniqueLink
     * @return void
     */
    public function updateExistingLink(UniqueLink $uniqueLink): void
    {
        $uniqueLink->update([
            'unique_link' => Str::random(10),
            'expires_at' => Carbon::now()->addDays(7),
        ]);
    }

    /**
     * Deactivate a unique link.
     *
     * @param UniqueLink $uniqueLink
     * @return void
     */
    public function deactivateLink(UniqueLink $uniqueLink): void
    {
        $uniqueLink->update(
            ['expires_at' => now()]
        );
    }

}
