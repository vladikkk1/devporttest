<?php

namespace App\Providers;

use App\Repositories\GameRepository;
use App\Repositories\Interfaces\GameRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class GameRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(GameRepositoryInterface::class, GameRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
