<?php

namespace App\Providers;

use App\Services\Interfaces\UniqueLinkServiceInterface;
use App\Services\UniqueLinkService;
use Illuminate\Support\ServiceProvider;

class UniqueLinkServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(UniqueLinkServiceInterface::class, UniqueLinkService::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
