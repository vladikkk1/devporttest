<?php

namespace App\Providers;

use App\Repositories\Interfaces\UniqueLinkRepositoryInterface;
use App\Repositories\UniqueLinkRepository;
use Illuminate\Support\ServiceProvider;

class UniqueLinkRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(UniqueLinkRepositoryInterface::class , UniqueLinkRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
