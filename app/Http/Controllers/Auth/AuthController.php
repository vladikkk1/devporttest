<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterUserRequest;
use App\Services\Interfaces\UniqueLinkServiceInterface;
use App\Services\Interfaces\UserServiceInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AuthController extends Controller
{

    protected UserServiceInterface $userService;
    protected UniqueLinkServiceInterface $uniqueLinkService;

    /**
     * AuthController constructor.
     *
     * @param UserServiceInterface $userService
     * @param UniqueLinkServiceInterface $uniqueLinkService
     */
    public function __construct(UserServiceInterface $userService, UniqueLinkServiceInterface $uniqueLinkService)
    {
        $this->userService = $userService;
        $this->uniqueLinkService = $uniqueLinkService;
    }

    /**
     * Display the registration form.
     *
     * @return View
     */
    public function showFormRegister(): View
    {
        return view('auth.register');
    }

    /**
     * Register a new user and generate a unique link.
     *
     * @param RegisterUserRequest $userRequest
     * @return RedirectResponse
     */
    public function register(RegisterUserRequest $userRequest): RedirectResponse
    {
        $data = $userRequest->validated();
        $user = $this->userService->create($data);
        $uniqueLink = $this->uniqueLinkService->generateUniqueLink($user->id);

        return redirect()->route('unique.link', $uniqueLink);
    }
}
