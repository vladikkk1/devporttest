<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\GameServiceInterface;
use Illuminate\Database\Eloquent\Casts\Json;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class GameController extends Controller
{
    protected GameServiceInterface $gameService;

    /**
     * GameController constructor.
     *
     * @param GameServiceInterface $gameService
     */
    public function __construct(GameServiceInterface $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * Play the game for the specified user.
     *
     * @param int $userId
     * @return JsonResponse
     */
    public function play(int $userId): JsonResponse
    {
        $data = $this->gameService->playGame($userId);
        return response()->json($data);
    }

}
