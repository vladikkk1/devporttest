<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\GameServiceInterface;
use App\Services\Interfaces\UniqueLinkServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class PageController extends Controller
{

    protected UniqueLinkServiceInterface $uniqueLinkService;
    protected GameServiceInterface $gameService;

    /**
     * PageController constructor.
     *
     * @param UniqueLinkServiceInterface $uniqueLinkService
     * @param GameServiceInterface $gameService
     */
    public function __construct(UniqueLinkServiceInterface $uniqueLinkService, GameServiceInterface $gameService)
    {
        $this->uniqueLinkService = $uniqueLinkService;
        $this->gameService = $gameService;
    }

    /**
     * Display the page for the specified unique link.
     *
     * @param string $uniqueLink
     * @return View|RedirectResponse
     */
    public function index(string $uniqueLink): View|RedirectResponse
    {
        $uniqueLink = urldecode($uniqueLink);

        $user = $this->uniqueLinkService->validateAndRetrieveUser($uniqueLink);

        if (!$user) {
            return abort(404);
        }

        return view('page', compact('user', 'uniqueLink'));
    }

    /**
     * Generate a unique link for the specified user.
     *
     * @param int $userId
     * @return RedirectResponse
     */
    public function generateUniqueLink(int $userId): RedirectResponse
    {
        return redirect()->route('unique.link', $this->uniqueLinkService->generateUniqueLink($userId));
    }

    /**
     * Deactivate the unique link for the specified user.
     *
     * @param int $userId
     * @return RedirectResponse
     */
    public function deactivateUniqueLink(int $userId): RedirectResponse
    {
        $this->uniqueLinkService->deactivateLink($userId);
        return redirect()->route('auth.showFormRegister');
    }

    /**
     * Retrieve the game history for the specified user.
     *
     * @param int $userId
     * @return JsonResponse
     */
    public function history(int $userId): JsonResponse
    {
        $data = $this->gameService->history($userId);
        return response()->json($data);
    }

}
