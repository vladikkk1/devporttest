<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [\App\Http\Controllers\Auth\AuthController::class, 'showFormRegister'])->name('auth.showFormRegister');
Route::post('/register', [\App\Http\Controllers\Auth\AuthController::class, 'register'])->name('auth.register');

Route::get('/{uniqueLink}', [\App\Http\Controllers\PageController::class, 'index'])->name('unique.link');
Route::post('/generate/{userId}', [\App\Http\Controllers\PageController::class, 'generateUniqueLink'])->name('unique.generate');
Route::post('/deactivate/{userId}', [\App\Http\Controllers\PageController::class, 'deactivateUniqueLink'])->name('unique.deactivate');
Route::post('/history/{userId}', [\App\Http\Controllers\PageController::class,'history'])->name('game.history');

Route::post('/play/{userId}', [\App\Http\Controllers\GameController::class,'play'])->name('game.play');
